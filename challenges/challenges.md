# Challenges

For every challenge the zip password is `hackthebox`

## htb/Baby_RE
> Show us your basic skills! (P.S. There are 4 ways to solve this, are you willing to try them all?) 

## htb/BabyCrypt
> Give me the key and take what's yours. 

## htb/Ouija
> You've made contact with a spirit from beyond the grave! Unfortunately, they speak in an ancient tongue of flags, so you can't understand a word. You've enlisted a medium who can translate it, but they like to take their time...

## htb/IRCWare
> During a routine check on our servers we found this suspicious binary, although when analyzing it we couldn't get it to do anything. We assume it's dead malware, but maybe something interesting can still be extracted from it? 
(There is a writeup available on documize)

## htb/Rebuilding
> You arrive on a barren planet, searching for the hideout of a scientist involved in the Longhir resistance movement. You touch down at the mouth of a vast cavern, your sensors picking up strange noises far below. All around you, ancient machinery whirrs and spins as strange sigils appear and change on the walls. You can tell that this machine has been running since long before you arrived, and will continue long after you're gone. Can you hope to understand its workings? 

## htb/YouCantCMe
> Can you see me?

## htb/Teleport
> You've been sent to a strange planet, inhabited by a species with the natural ability to teleport. If you're able to capture one, you may be able to synthesise lightweight teleportation technology. However, they don't want to be caught, and disappear out of your grasp - can you get the drop on them? 

## htb/Indefinite
> You hold in one hand an encrypted datastream, and in the other the central core of a Golden Fang communications terminal. Countless spies have risked their lives to steal both the encrypted attack plans, and the technology used to conceal it, and bring them to you for expert analysis. To your horror, as you turn the screws on the core, its defense mechanisms spring to life, concealing and covering its workings. You have minutes to spare before the device destroys itself - can you crack the code?

# Qiling advised:
## htb/pseudo
> Do you have enough permissions to get the flag? 

## htb/AntiFlag
> Flag? What's a flag?

## htb/Headache
> make the flag. 

