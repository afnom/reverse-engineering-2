#include <stdio.h>
#include <string.h>
#include <unistd.h>

__attribute__((constructor)) void i () {
    setvbuf(stdin, 0, _IONBF, 0);
    setvbuf(stdout, 0, _IONBF, 0);
    setvbuf(stderr, 0, _IONBF, 0);
}

int main(){
    char *password = "ANFOM{eventually_means_the_m6_toll_needs_to_be_maintained_after_the_heat_death_of_the_universe}";
    char input[96];

    puts("Enter password: ");
    fgets(input, 96, stdin);

    int acc = 1;
    int check = 0;

    printf("Checking: ");
    for (int i = 0; i < 96; i++) {
        printf(".");
        check |= password[i] != input[i];
        acc *= (i+1);
        sleep(acc);
    }

    if (check) {
        puts("wrong!");
    } else {
        puts("Correct!");
    }
}