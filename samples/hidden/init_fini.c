#include <stdio.h>

__attribute__((constructor)) void i () {
    puts("In Constructor");
}

__attribute__((destructor)) void f () {
    puts("In Destructor");
}

int main() {
    puts("In Main");
    return 0;
}