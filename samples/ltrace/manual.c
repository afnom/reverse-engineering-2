#include <string.h>
#include <stdio.h>


char *FLAG = "MANUAL";

int main() {
    int check = 0;
    char input[7];

    puts("password: ");
    fgets(input, 7, stdin);

    for (int i = 0; i < 7; i++) {
        check |= (FLAG[i] != input[i]);
    }

    if (check) {
        puts("Wrong!");
    } else {
        puts("Correct!");
    }
}