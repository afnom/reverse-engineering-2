#include <stdio.h>

int AddTwoNumber(int, int);
__asm(
  ".globl AddTwoNumber\n"
  ".text\n"
  "AddTwoNumber:\n"
  "  ud2\n"
  "  ret\n"
);

int MultTwoNumber(int, int);
__asm(
  ".globl MultTwoNumber\n"
  ".text\n"
  "MultTwoNumber:\n"
  "  ud2\n"
  "  ret\n"
);

int Debuggee() {
  puts("Debuggee (child starting)"); fflush(stdout);

  // 345
  printf("result: %i\n", AddTwoNumber(123, 222));

  printf("result: %i\n", MultTwoNumber(15, 23));

  puts("Debuggee (child exiting)"); fflush(stdout);

  return 0;
}