#include <stdio.h>
#include <stdlib.h>
#include <sys/ptrace.h>

__attribute__((constructor)) void f () {
    int errno = ptrace(PTRACE_TRACEME, 0, 0, 0);
    if (errno) {
        puts("Being Debugged");
        exit(-1);
    }
}

int main() {
    puts("Not being debugged");
}