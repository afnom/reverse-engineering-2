# Google CTF 2022

## reversing - eldar

We found this app on a recent Ubuntu system, but the serial is missing from `serial.c`. Can you figure out a valid serial?

Just put it into `serial.c` and run the app with `make && make run`.

Thanks!

Note: you don't have to put anything else other than the serial into `serial.c` to solve the challenge.
