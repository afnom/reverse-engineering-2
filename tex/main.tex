\documentclass[a4paper,12pt]{article}

\usepackage[margin=0.5in]{geometry}

\usepackage{xcolor}
\definecolor{url}{HTML}{006600}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,   
    urlcolor=url
    }

\usepackage{minted}


\author{0' -- 74F3}
\title{AFNOM - REV 2}
\date{February 8th}

\begin{document}
\maketitle

\section*{Overview}

In this second part of our reverse engineering series, we will be exploring binary analysis. There is not a requirement to understand everything from the first session, that was purely to introduce how the tools we will be using works.

When reverse engineering there are two types of techniques, static analysis and dynamic analysis. These types relate to how you interact with a given sample, In most cases, we will use a combination of static and dynamic analysis techniques.

When performing this analysis we may run into "fake flags", these are flags that are not accepted by the challenge platform. Typically, the binary will validate the flag and as such the flag should be input into the binary without any reversing techniques being applied.

As a final thing to mention, when reverse engineering software from untrusted sources ensure that you use a dedicated environment that is isolated from any networks. In our sessions we will always be using ``defanged" samples, these are samples that do not have adverse effects on your computer.

As my compiled binaries might not work on your computer, you will have to compile them yourself. To do this, I have provided a \texttt{Makefile} to use this you can simply run \texttt{make all} or \texttt{make <folder>}:

\inputminted[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\footnotesize
]{text}{code/make/all-makefile}

You can also run \texttt{make clean} to delete all of the compiled binaries:

\inputminted[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\footnotesize
]{text}{code/make/clean-makefile}

\section*{Static Analysis}

Static analysis describes the category of techniques where the binary/sample is not executed. This is generally thought of as the ``safest'' method of reverse engineering, as any malicious code is not being directly ran. Due to this, it is always a good idea to have a quick once over using static methods before using dynamic analysis techniques.

\subsection*{strings / grep}

When a program is compiled, variables such as strings will need found inside of the program. In some cases, we can simply use strings to dump the flag from the binary.

\inputminted[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\footnotesize,
    linenos
]{text}{code/strings/strings-baby-re}

\subsection*{Ghidra}

\texttt{Ghidra} is a free open-source reverse engineering tool created by the NSA which features both a disassembler and decompiler as well as a debugger. As Ghidra is open-source there are a range of plugins that help to reverse engineer given binaries, an examples of this is \href{https://github.com/felberj/gotools}{gotools which helps in reversing go binaries}.

However, sometimes we may want to use separate tools for numerous reasons. Maybe the representation is clearer or there is a better compatibility with a given binary, etc.

\subsection*{Cutter}

Cutter is another free open-source reverse engineering tool which features both a disassembler and decompiler. I have personally found that Cutter has greater compatibility for non-x86/x64 binaries, however you may have to describe the type of file you are passing into Cutter.

\subsection*{dogbolt}

\href{https://dogbolt.org/}{Dogbolt is a website which allows you to compare decompilers}. You can upload custom binaries or select from some provided samples and look at how different decompilers interact with the binary.

\subsection*{objdump}
% https://ctf.harrisongreen.me/2022/googlectf/eldar/

There are some instances where tools such as Ghidra or Cutter are not as useful to us. One example of this is the \href{https://github.com/google/google-ctf/tree/master/2022/quals/rev-eldar}{eldar challenge from Google CTF 2022}. In this challenge, a weird machine is injected into our binary which verifies our flag.

We are given a \texttt{serial.c} file in which we input the flag. This file is compiled into a shared object file which is loaded by a given binary \texttt{eldar}.

\inputminted[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\footnotesize,
    linenos
]{text}{code/eldar/serial.c}

We can put the \texttt{eldar} binary into Ghidra, however after looking at the main function it doesn't appear to do much of anything. We can try putting this into Cutter instead, as we have seen last week tools such as pycdc can have bugs, however we get very similar output. 

Ghidra:
\inputminted[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\footnotesize
]{text}{code/eldar/ghidra-main}

Cutter:
\inputminted[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\footnotesize
]{text}{code/eldar/cutter-main}

In cases like this, it is useful to analyse the files on a lower level. We can use tools such as \texttt{objdump} to do this.

Elf files are made of multiple different sections, these all play different roles. If you want a deeper dive into the format of elf files \href{https://linuxhint.com/understanding_elf_file_format/}{you can have a look here}. We can use \texttt{objdump} to list the sections of the binary.

\inputminted[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\footnotesize
]{text}{code/eldar/objdump-h-eldar}

It is also useful to have a look at other examples to help pinpoint anything that stands out, so below I have provided the same output for \texttt{libc.so.6} and \texttt{/bin/sh}.

\inputminted[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\footnotesize
]{text}{code/eldar/objdump-h-libc}

\inputminted[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\footnotesize
]{text}{code/eldar/objdump-h-bin-sh}

If we were to compare the \texttt{.rela.dyn} sections of each sample, we would see that the \texttt{eldar} binary is much larger (\texttt{00255ac8} compared to \texttt{bin/sh 00008bc8} or \texttt{libc.so.6 00000118}). This is extremely suspicious and would be a good line of enquiry. 

This challenge is quite difficult and so if you want to read a write-up  \href{https://ctf.harrisongreen.me/2022/googlectf/eldar/}{here is one that uses similar techniques to the ones shown in the reversing 1 session}. (This was the write-up that inspired my solution for the \texttt{boxast} challenge)

\section*{Dynamic Analysis}
Sometimes static analysis alone is not powerful enough to reverse engineer binaries, a binary might self-modify their instructions and so we need a new class techniques.

Dynamic analysis describes the category of techniques where the binary/sample is executed. There are techniques that can be used to detect dynamic analysis, so it is important to be aware of these detection methods.

Malware, \href{https://www.wired.com/2017/05/accidental-kill-switch-slowed-fridays-massive-ransomware-attack/}{such as Wannacry}, typically contains a "killswitch" which protects the developer from executing on their computers. The malware might detect if it is being ran in a virtual machine, a debugger, etc and alter its functionality. 

\subsection*{ltrace}

\texttt{ltrace} is a cli program that simply logs all calls to library functions. This is a good first try when we run into reverse engineering a binary.

I have provided two files in \texttt{./samples/ltrace/}, \texttt{strcmp.c} and \texttt{manual.c} where we can explore this.

In \texttt{strcmp.c} I use the library function \texttt{strncmp} to compare the flag and user input. We can use \texttt{ltrace} to view the call to \texttt{strncmp}.

\inputminted[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\footnotesize,
    linenos
]{c}{code/ltrace/strcmp.c}

\inputminted[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\footnotesize
]{text}{code/ltrace/strcmp-ltrace-wrong}

We can see that the first argument to strncmp is ``STRCMP'' and the second argument is our input. We can now try inputting ``STRCMP''

\inputminted[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\footnotesize
]{text}{code/ltrace/strcmp-ltrace-correct}

In \texttt{manual.c}, however, I loop through each character in the FLAG and compare it to the corresponding character inputted by the user. As I am not using a library function, I will not be able to see the comparison in the output of \texttt{ltrace}.

\inputminted[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\footnotesize,
    linenos
]{c}{code/ltrace/manual.c}

\inputminted[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\footnotesize
]{text}{code/ltrace/manual-ltrace}

This time, we cannot see the ``FLAG'' in the output of \texttt{ltrace}. We will have to find another method of getting the flag.

\subsection*{GDB}

\texttt{GDB} refers to the GNU project debugger. It is a tool that we can use to inspect a binary at runtime. We can access the program memory, registers, etc.

In most cases, when people are using \texttt{GDB} they are using plugins such as \href{https://github.com/hugsy/gef}{GEF}, \href{https://github.com/longld/peda}{PEDA} or \href{https://github.com/pwndbg/pwndbg}{pwndbg}. This is because they either add new features or have a better user interface. I will personally be using a fork of GEF due to some of the features it provides for pwn challenges, but there are scripts such as \href{https://github.com/apogiatzis/gdb-peda-pwndbg-gef}{gdb-peda-pwndbg-gef} that will install all of them at once and allow you to run each with separate commands. If you are just starting out, I would recommend playing around with each one and finding which one you are most comfortable with.

\texttt{GDB} has a massive range of functions so your best bet for using it would be a combination of \href{https://guyinatuxedo.github.io/02-intro_tooling/gdb-gef/index.html}{tutorials} and the documentation accessible through the \texttt{help} command.

\inputminted[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\footnotesize
]{text}{code/gdb/gdb-help}

Another feature of \texttt{GDB} is \texttt{gdbscript}. These are text files that list a series of commands to be run when a file is loaded. Below I have created a gdbscript file at \texttt{samples/GDB/manual} that outputs the flag from the \texttt{manual} binary.

\inputminted[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\footnotesize
]{text}{code/gdb/gdbscript}

\inputminted[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\footnotesize
]{text}{code/gdb/gdb-run}

\subsection*{ptrace}
There are limitations, however, when using tools such as \texttt{GDB} or \texttt{ltrace} due to their implementation. To demonstrate this, I have included the \texttt{ptrace/debugme} binary.

\inputminted[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\footnotesize,
    linenos
]{c}{code/ptrace/debugme.c}

\texttt{GDB} and \texttt{ltrace} make use of ptrace, a series of library functions that allow for tracing a program's execution. Because of how ptrace is implemented, a program can only ever have 1 ptrace debugger at a time.

This can be leveraged by malware authors, or challenge authors, to detect if the binary is being debugged and alter functionality as a result. There are a couple methods of varying difficulty that we can use to bypass this.

\subsection*{LD\_PRELOAD}

\texttt{LD\_PRELOAD} refers to an environment variable rather than a tool though this does not make this technique any less powerful. \texttt{LD\_PRELOAD} allows us to force a given library file to be loaded before any other libraries. This allows us to essentially overwrite function definitions with our own custom implementation.

The clearest demonstration of this is with the \texttt{sleep} function. The \texttt{sleep} function simply takes the number of seconds the binary should sleep for as an argument. In the below code, it takes more than $10^{134}$ years to check the flag. We don't have that much time.

\inputminted[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\footnotesize,
    linenos
]{c}{code/LD_PRELOAD/challenge.c}

In this case, I have intentionally used the sleep function to greatly increase the runtime of the program. We can make use of \texttt{LD\_PRELOAD} to overwrite the sleep function.

To overwrite functions, we need to be careful to match the inputs and outputs of the function we are overwriting. To get this, we can use the \href{https://www.kernel.org/doc/man-pages/}{man page} of the function.

\inputminted[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\footnotesize
]{text}{code/LD_PRELOAD/man-sleep}

By using this man page, we can get the following source code:

\inputminted[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\footnotesize,
    linenos
]{c}{code/LD_PRELOAD/nosleep.c}

The other thing to note is that we have to compile this into a shared object instead of a binary, this is so that we can load our custom sleep function as a library.

\inputminted[
    frame=lines,
    framesep=2mm,
    baselinestretch=1.2,
    fontsize=\footnotesize
]{text}{code/LD_PRELOAD/gcc-lib}

We can now run the binary, whilst specifying the value of the \texttt{LD\_PRELOAD} environment variable. The check should be performed almost instantaneously.

As a side note, you should be very careful with \texttt{LD\_PRELOAD} as it can overwrite \textbf{ANY} library function. I have broken virtual machines by enforcing the environment variable globally.

This technique only works for library functions, if we want to alter methods which are compiled into the binary we will need to explore other techniques such as binary patching or tools such as \texttt{Qiling}.

\subsection*{Qiling}

\texttt{Qiling} is a binary instrumentation framework, it allows us to emulate a given binary. As it emulates the binary, we can read memory and registers without having to call ptrace. In addition to this, it can be used to emulate a range of operating systems and architectures.

There are downsides to the binary being emulated, not all system/library calls may be implemented. This means we may have to patch in our own instructions.

If you want to play around with this more there are writeups available here:

\begin{itemize}
    \item \href{https://n1ght-w0lf.github.io/tutorials/qiling-for-malware-analysis-part-1/}{Qiling for malware analysis part 1}
    \item \href{https://n1ght-w0lf.github.io/tutorials/qiling-for-malware-analysis-part-2/}{Qiling for malware analysis part 2}
    \item \href{https://kernemporium.github.io/posts/unpacking/}{Automatic unpacking with Qiling framework}
\end{itemize}

\end{document}