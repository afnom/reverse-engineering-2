#include <string.h>
#include <stdio.h>


char *FLAG = "STRCMP";

int main() {
    int check = 0;
    char input[7];

    puts("password: ");
    fgets(input, 7, stdin);

    check = strncmp(FLAG, input, 8);
    if (check != 0) {
        puts("Wrong!");
    } else {
        puts("Correct!");
    }
    return !!check;
}